# 校园百事通微信小程序

## 界面展示

|                    首页                    |                    列表                    |                   发布帖子                   |
| :--------------------------------------: | :--------------------------------------: | :--------------------------------------: |
| ![1687261070156](img/1687261070156.png)  | ![1687261183578](img/1687261183578.png)  |  ![S5K9TKRY2N8{0DVYR4%(W](img/发布帖子.png)  |
|                   发布成功                   |                   帖子搜索                   |                   商家小店                   |
|          ![发布成功](img/发布成功.png)           | ![mmexport1687262091456](img/mmexport1687262091456.png) | ![mmexport1687262069636](img/mmexport1687262069636.png) |
|                   店面详情                   |                   联系商家                   |                   分享商家                   |
| ![mmexport1687262076976](img/mmexport1687262076976-7262771481.png) | ![mmexport1687262080675](img/mmexport1687262080675.png) | ![mmexport1687262084489](img/mmexport1687262084489.png) |

|                   帖子评论                   |                   分享帖子                   |                   个人页面                   |
| :--------------------------------------: | :--------------------------------------: | :--------------------------------------: |
| ![mmexport1687262341974](img/mmexport1687262341974.png) | ![1687263062341](img/1687263062341.png) | ![mmexport1687262056177](img/mmexport1687262056177.png) |



## 项目概述

本项目是一个综合性的校园社区平台，致力于为学生提供一个便捷、实用、互动性强的信息交流空间。平台涵盖了校园、表白墙、失物招领、二手交易、论坛、兼职、拼车、活动等多个分类，满足学生在不同场景下的需求。平台具备发布帖子、帖子收藏、帖子转发、帖子的增删改查等功能，支持匿名发布，管理员可以封禁用户并进行前台管理。此外，平台还提供了系统消息功能以及商家服务功能，如首页显示商家列表，查看小店商品，联系小店、分享店铺等。

## 项目功能介绍

- 多样化的帖子分类

平台提供了丰富的帖子分类，包括校园、表白墙、失物招领、二手交易、论坛、兼职、拼车、活动等，用户可以根据自身需求在不同分类下发布信息或浏览其他用户的帖子。

- 帖子管理功能

用户可以在平台上轻松发布帖子，并进行收藏、转发、编辑和删除操作。支持匿名发布，保护用户隐私。同时，管理员具有封禁用户和前台管理帖子的权限，确保平台秩序。

- 系统消息功能

平台具备系统消息功能，可向用户发送实时通知，如回复提醒、活动通知等，帮助用户及时了解平台动态。

- 商家服务功能

平台为商家提供了便捷的服务功能，包括首页展示商家列表、查看小店商品、联系小店和分享店铺。用户可以在平台上直接与商家互动，实现一站式购物体验。



## 运行环境

开发工具：IDEA（后端编译器）、微信开发者工具（前端编译器）

JDK版本：1.8

NodeJs版本：8.0以上

数据库：MySQL5.6以上

项目管理：Maven（用于springboot项目下载依赖）



## 环境安装

每个环境安装均在csdn专栏有详细讲解，欢迎给博主和专栏点个关注:blue_heart:

[点击进入专栏](https://blog.csdn.net/m0_46381569/category_12335024.html)

![1687272181623](img/1687272181623.png)

## 后端运行

建议查看本人csdn文章，里面详细讲解了IDEA启动Springboot项目

[点击进入文章：IDEA运行SpringBoot项目](https://blog.csdn.net/m0_46381569/article/details/131121562)

### 导入Mysql脚本

> [Navicat导入数据教程](https://blog.csdn.net/m0_46381569/article/details/131121357)

数据文件是在主目录下的school.sql，运行后就能看到school数据库，后续需要在后端文件修改yml文件配置，设置为自己数据库的账号和密码。

![1687244560455](img/1687244560455.png)

注： 管理员以及禁用的功能(数据库表mango_user中的user_is_admin为2是管理员,user_allow为1正常使用,其他数字禁止用户使用程序功能)，管理员可以修改用户的帖子以及删除,在帖子页面,点击头像可以查看用户的信息并可以禁用Ta,可以回复消息给Ta]



### IDEA打开后端文件

File - open -选择源码文件下的文件夹（这里需要打开CommunitySystem)

![1687272277113](img/1687272277113.png)

![1687244763901](img/1687244763901.png)

这样就成功导入源码



### 配置OSS图片服务

> 本系统的图片存放在了阿里云OSS服务器中，如果继续使用本人提供的oss地址则不需要进行下面步骤。
>
> 有能力的还是进行个人OSS配置，可以更换图片。
>
> 目的是上传图片到OSS服务器后，就能获得图片访问的HTTP链接，软件就能访问获得图片显示。

 [oss购买地址](https://common-buy.aliyun.com/?spm=5176.21213303.5694434980.3.952653c9qzxZFz&commodityCode=ossbag&request=%7B%22region%22%3A%22china-common%22%7D&accounttraceid=62b0a4bfe0e6441fb95722022c05b972skez) ，默认配置就行，几块钱。创建bucket，请将bucket默认权限改为公共读写,否则外界无法访问

![1687245658396](img/1687245658396.png)



然后需要去后台源码/Mango/src/main/java/work/huangxin/mango/util/isDelete/IsDelete.java添加你自己的oss信息(删除oss的文件的操作)



### 添加小程序信息

因为微信小程序登录需要用到appid和密钥，登陆微信公众平台查看小程序的appid和密钥，然后找到后台源码Controller层的LoginController.java修改你的appid和密钥

![1687245932727](img/1687245932727.png)



### 环境配置

主要是进行配置Maven、JDK等。

参考[ 点击进入文章：IDEA运行SpringBoot项目](https://blog.csdn.net/m0_46381569/article/details/131121562?csdn_share_tail=%7B%22type%22%3A%22blog%22%2C%22rType%22%3A%22article%22%2C%22rId%22%3A%22131121562%22%2C%22source%22%3A%22m0_46381569%22%7D)



### 后端项目启动

正常项目环境搭建完成，右上角会有绿色三角形图标，点击此处即运行！

如果右上角没有，找到文件MiniProgramRunApp，如下方式二，也可运行项目

![1687246056856](img/1687246056856.png)

出现下图表示启动成功

![1687246167232](img/1687246167232.png)

开启后端服务之后，就可以打开前端了



## 前端运行

下载安装微信开发者工具，打开前台源码目录，输入appid，选择不使用云服务

就能显示前端界面啦！！

![1687247044717](img/1687247044717.png)

如果你使用自己的oss地址：

- 修改app.js文件去配置oss的地址（imageUrl）
- 前台util/config.js配置阿里云oss(如果有自己需求可以直接在后台增加上传的代码)

到这里前后端就启动完成了，后续修改数据表的内容，重新刷新页面就能看到变化。

最后，喜欢的欢迎点点star:blue_heart:



## 源码获取

源码获取关注个人公众号：楠哥毕设帮

提供个人二开项目源码、部署讲解、论文指导~

![qrcode1686724549589](https://gitee.com/xie-baoming/commnity/raw/master/ReadME.assets/qrcode1686724549589.jpg)





